FROM php:8.1-alpine
COPY data/install_composer.sh install_composer.sh
RUN ./install_composer.sh && \
    mv composer.phar /usr/local/bin/composer && \
    composer create-project symfony/symfony-demo my_project
EXPOSE 8000
CMD ["php","-S","0.0.0.0:8000","-t","my_project/public/"]
